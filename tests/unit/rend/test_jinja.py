"""
    tests.unit.rend.test_jinja
    ~~~~~~~~~~~~~~

    Unit tests for the jinja renderer
"""
import pytest

import rend.exc


@pytest.mark.asyncio
async def test_jinja(mock_hub, hub):
    """
    test rend.jinja.render renders correctly
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render

    ret = await mock_hub.rend.jinja.render('{% set test = "itworked" %}{{ test }}')
    assert ret == "itworked"


@pytest.mark.asyncio
async def test_jinja_bytes(mock_hub, hub):
    """
    test rend.jinja.render renders correctly with bytes data
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    ret = await mock_hub.rend.jinja.render(b'{% set test = "itworked" %}{{ test }}')
    assert ret == "itworked"


@pytest.mark.asyncio
async def test_jinja_undefined(mock_hub, hub):
    """
    test rend.jinja.render when there is an undefined error
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    with pytest.raises(rend.exc.RenderException) as exc:
        await mock_hub.rend.jinja.render("{{ hello.test }}")
    assert exc.value.args[0] == "Jinja variable 'hello' is undefined"


@pytest.mark.asyncio
async def test_jinja_syntax(mock_hub, hub):
    """
    test rend.jinja.render when there is a syntax error
    """
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    with pytest.raises(rend.exc.RenderException) as exc:
        await mock_hub.rend.jinja.render("{% test % }")
    assert exc.value.args[0] == "Jinja syntax error Encountered unknown tag 'test'."
