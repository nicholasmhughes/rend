"""
    tests.unit.rend.test_yaml
    ~~~~~~~~~~~~~~

    Unit tests for the yaml renderer
"""
import pytest

import rend.exc


@pytest.mark.parametrize("data", [b"test: one", "test: one"])
def test_yaml(mock_hub, data, hub):
    """
    test rend.yaml.render renders correctly
    """
    mock_hub.rend.yaml.render = hub.rend.yaml.render
    ret = mock_hub.rend.yaml.render(data)
    assert isinstance(ret, dict)
    assert ret["test"] == "one"


def test_yaml_scanner_exc(mock_hub, hub):
    """
    test rend.yaml.render when there is a scanner error
    """
    mock_hub.rend.yaml.render = hub.rend.yaml.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.yaml.render("test:\none")
    assert exc.value.args[0] == "Yaml render error: could not find expected ':'"


def test_yaml_parser_exc(mock_hub, hub):
    """
    test rend.yaml.render when there is a parser error
    """
    mock_hub.rend.yaml.render = hub.rend.yaml.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.yaml.render("- !-!str just a string")
    assert exc.value.args[0] == "Yaml render error: found undefined tag handle"


def test_yaml_constructor_exc(mock_hub, hub):
    """
    test rend.yaml.render when there is a contructor error
    """
    mock_hub.rend.yaml.render = hub.rend.yaml.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.yaml.render("- !!!str just a string:one")
    assert (
        exc.value.args[0] == "Yaml render error: could not determine "
        "a constructor for the tag 'tag:yaml.org,2002:!str'"
    )


def test_duplicate_keys(mock_hub, hub):
    data = """foo: bar
foo: bar
    """
    mock_hub.rend.yaml.render = hub.rend.yaml.render
    with pytest.raises(rend.exc.RenderException) as exc:
        ret = mock_hub.rend.yaml.render(data)
    assert exc.value.args[0] == "Yaml render error: found conflicting ID 'foo'"
